#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import os
import os.path
import multiprocessing
import time
import Queue
import random
import re
import logging

from multiprocessing import Process, Lock, cpu_count

from smartrm2.trash import Trash
from smartrm2.trashfile import get_trashfile_by_file
from smartrm2.helpers.file_system_helper import check_path_for_remove, get_correct_subpaths
from smartrm2.rmerror import RMError


class FileRemover(Process):
    def __init__(self, root, regex, trash, dry_run, operation_id, index_lock, dirs):
        Process.__init__(self)
        self.root = root.rstrip(os.sep)
        self.regex = regex
        self.trash = trash
        self.dry_run = dry_run
        self.operation_id = operation_id
        self.index_lock = index_lock
        self.dirs = dirs

    def run(self):
        while True:
            dirpath = self.dirs.get()

            if not os.access(dirpath, os.R_OK):
                print 'Access denied : ' + dirpath
                self.dirs.task_done()
                continue

            for filename in os.listdir(dirpath):
                subpath = os.path.join(dirpath, filename)
                if os.path.isdir(subpath):
                    self.dirs.put(subpath)

            for filename in os.listdir(dirpath):
                subpath = os.path.join(dirpath, filename)
                if os.path.isfile(subpath):
                    regexpath = subpath[len(os.path.dirname(self.root)):]
                    if _is_equal_regex(regexpath, self.regex):
                        try:
                            _rmfile(
                                path=subpath, 
                                trash=self.trash, 
                                dry_run=self.dry_run, 
                                operation_id=self.operation_id,
                                index_lock=self.index_lock,
                            )
                        except RMError as exc:
                            print exc.message

            self.dirs.task_done()


def remove_tree(path, regex='.*', trash=None, dry_run=False, processes_count=cpu_count(), operation_id=None):
    logger = logging.getLogger('smartrm2')

    correct_subpaths = [ subpath for subpath in get_correct_subpaths(path)
                         if _is_equal_regex(subpath, regex) ]

    if trash and trash.config.maxnumber:
        if trash.get_number_of_files() + len(correct_subpaths) > \
           trash.config.maxnumber:
            raise RMError('Max number files in trash')

    if trash and trash.config.maxsize:
        size = 0
        for subpath in correct_subpaths:
            size += os.path.getsize(subpath)

        if trash.get_size_of_trash() + size > trash.config.maxsize:
            raise RMError('Not enough capacity in trash')

    if not operation_id:
        operation_id = get_new_operation_id()

    index_lock = Lock()

    dirs = multiprocessing.JoinableQueue()

    removers = [ 
        FileRemover(path, regex, trash, dry_run, 
                    operation_id, index_lock, dirs) 
        for i in range(processes_count) 
    ]

    logger.debug('Created {0} process for removing {1}'\
                 .format(processes_count, path))

    [ remover.start() for remover in removers ]

    dirs.put(path)
    dirs.join()

    logger.debug('All tasks completed')

    [ remover.terminate() for remover in removers ]
    [ remover.join() for remover in removers ]

    logger.debug('All processes joined')

    path = path.rstrip(os.sep)
    for root, dirs, files in os.walk(path, topdown=False):
        if _is_equal_regex(root[len(os.path.dirname(path)):], regex):
            try:
                _rmfile(root, trash, dry_run, operation_id, index_lock)
            except RMError as exc:
                print exc.message

    logger.debug('All directories removed')

    if trash:
        trash._init_files()

    return operation_id


def remove_file(path, trash=None, dry_run=False, operation_id=None):
    if not operation_id:
        operation_id = get_new_operation_id()
        
    _rmfile(path, trash, dry_run, operation_id, Lock())

    return operation_id


def get_new_operation_id():
    return random.randint(10 ** 30, 10 ** 31 - 1)


def _rmfile(path, trash, dry_run, operation_id, index_lock):
    logger = logging.getLogger('smartrm2')

    check_path_for_remove(path)

    if not dry_run:
        trashfile = get_trashfile_by_file(path)
        trashfile.operation_id = operation_id

        if trash:
            filename = os.path.basename(path)
            with index_lock:
                index = trash.get_free_index_and_reserve_place(filename)
            trash.move_file_to_trash_by_trashfile(trashfile, index)
        else:
            if os.path.isfile(path):
                os.remove(path)
            elif os.path.isdir(path):
                os.rmdir(path)

    logger.info('{0} removed'.format(path))


def _get_subpaths(path):
    paths = []
    for root, dirs, files in os.walk(path):
        for file in files:
            paths.append(os.path.join(root, file))

        paths.append(root)

    return paths


def _is_equal_regex(path, regex):
    names = path.strip(os.sep).split(os.sep)

    for name in names:
        res = re.match(regex, name)
        if res is not None:
            if res.group(0) == name:
                return True

    return False
