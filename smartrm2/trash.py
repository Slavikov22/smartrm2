#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import os
import os.path
import logging
import time
import shutil

from smartrm2.trashfile import get_trashfile_by_file, get_trashfile_by_info, create_info
from smartrm2.rmerror import RMError
from smartrm2.validators import get_validator, ValidationError


class Trash(object):
    def __init__(self, config):
        self.logger = logging.getLogger('smartrm2')
        self.logger.debug('Prepare to initialized trash')

        self.config = config

        self.path = config.trashpath
        self.files_path = os.path.join(self.path, 'files')
        self.infos_path = os.path.join(self.path, 'info')

        self._init_directory(self.path)
        self._init_directory(self.files_path)
        self._init_directory(self.infos_path)

        self._init_files()

        self.start_autoclear()

        self.logger.info('Trash initialized')

    def move_file_to_trash(self, path, index=None):
        trashfile = get_trashfile_by_file(path)
        self.move_file_to_trash_by_trashfile(trashfile, index)

    def move_file_to_trash_by_trashfile(self, trashfile, index=None):
        self.logger.debug('File {0} prepare to replaced to trash'\
                          .format(trashfile.oldpath))

        if _is_paths_intersect(trashfile.oldpath, self.path):
            raise RMError('Try to delete trash files')

        self._check_file(trashfile.oldpath)

        filename = os.path.basename(trashfile.oldpath)

        if index is None:
            index = self.get_free_index(filename)

        self._prepare_index_for_file(filename, index)

        newname = filename + '[{0}]'.format(index)

        create_info(
            infopath=os.path.join(self.infos_path, newname + '.trashinfo'),
            trashfile=trashfile,
        )

        os.rename(trashfile.oldpath, os.path.join(self.files_path, newname))

        self.files[filename][index] = trashfile

        if os.path.exists(os.path.join(self.files_path, newname + '.reserved')):
            os.remove(os.path.join(self.files_path, newname + '.reserved'))

        self.logger.debug('File {0} replaced to trash'\
                          .format(trashfile.oldpath))


    def restore_trashfile(self, trashfile):
        self.logger.debug('File {0} prepare to restored'\
                          .format(trashfile.oldpath))

        filepath, infopath = self.get_paths_of_trashfile(trashfile)

        if os.path.isdir(filepath) and os.path.isdir(trashfile.oldpath):
            os.rmdir(filepath)
        else:
            if self.config.restore_policy == 'aggressive':
                path = trashfile.oldpath
                while path != '/':
                    if os.path.isfile(path):
                        os.remove(path)
                    path = os.path.dirname(path)

                if os.path.isdir(trashfile.oldpath):
                    shutil.rmtree(trashfile.oldpath)
            else:
                if os.path.exists(trashfile.oldpath):
                    raise RMError("Can't rewrite file on {0}"\
                                  .format(trashfile.oldpath))

                path = trashfile.oldpath
                while path != '/':
                    if os.path.isfile(path):
                        raise RMError("Can't rewrite file on {0}"\
                                      .format(trashfile.oldpath))
                    path = os.path.dirname(path)

            os.renames(filepath, trashfile.oldpath)
        
        os.remove(infopath)

        filename = os.path.basename(trashfile.oldpath)
        index = self.files[filename].index(trashfile)

        self.files[filename][index] = None

        self._init_directory(self.files_path)

        self.logger.info('File {0} restored'.format(trashfile.oldpath))

    def delete_file(self, trashfile):
        self.logger.debug('Prepare to delete {0}'.format(trashfile.oldpath))

        filepath, infopath = self.get_paths_of_trashfile(trashfile)
            
        if os.path.isfile(filepath):
            os.remove(filepath)
        elif os.path.isdir(filepath):
            os.rmdir(filepath)

        os.remove(infopath)

        filename = os.path.basename(trashfile.oldpath)
        index = self.files[filename].index(trashfile)

        self.files[filename][index] = None

        self.logger.info('File {0} deleted'.format(trashfile.oldpath))

    def get_free_index(self, filename):
        index = 0

        while True:
            newpath = os.path.join(self.files_path, 
                                   filename + '[{0}]'.format(index))
            try:
                if (os.path.exists(newpath) or
                   os.path.exists(newpath + '.reserved') or 
                   self.files[filename][index] == 'reserved'):
                    index += 1
                    continue
            except KeyError:
                break
            except IndexError:
                break
            except AttributeError:
                break
            break

        return index

    def get_free_index_and_reserve_place(self, filename):
        index = self.get_free_index(filename)
        self._reserve_place_for_file(filename, index)

        return index

    def start_autoclear(self):
        self.logger.debug('Prepare to autoclear')

        if self.config.autoclear_policies:
            for filename in self.files.keys():
                for trashfile in self.files[filename]:
                    if trashfile:
                        for policy in self.config.autoclear_policies.keys():
                            try:
                                validator = get_validator(policy)
                            except RMError:
                                pass
                            else:
                                value = self.config.autoclear_policies[policy]
                                try:
                                    validator(trashfile, value)
                                except ValidationError:
                                    self.delete_file(trashfile)

        self.logger.debug('Autoclear successful')


    def clear(self):
        for filename in self.files.keys():
            for trashfile in self.files[filename]:
                self.delete_file(trashfile)

        self.logger.debug('Trash cleared')

    def get_number_of_files(self):
        number = 0
        for filename in self.files.keys():
            for trashfile in self.files[filename]:
                if trashfile:
                    number += 1

        return number

    def get_size_of_trash(self):
        size = 0
        for filename in self.files.keys():
            for trashfile in self.files[filename]:
                if trashfile:
                    size += trashfile.size

        return size

    def get_trashfiles(self):
        trashfiles = []
        for filename in self.files.keys():
            for trashfile in self.files[filename]:
                if trashfile:
                    trashfiles.append(trashfile)

        return trashfiles

    def get_paths_of_trashfile(self, trashfile):
        filename = os.path.basename(trashfile.oldpath)
        index = self.files[filename].index(trashfile)

        filepath = os.path.join(self.files_path, filename + '[{0}]'.format(index))
        infopath = os.path.join(self.infos_path, filename + '[{0}]'.format(index) + '.trashinfo')

        return filepath, infopath

    def show(self):
        print 'Trashpath:', self.path
        print 'Maxsize:', self.config.maxsize
        print 'Maxnumber:', self.config.maxnumber
        print ''

        if self.config.autoclear_policies:
            print 'Autoclear policies:'
            for policy in self.config.autoclear_policies.keys():
                print policy + ' : ' + str(self.config.autoclear_policies[policy])
            print ''

        if self.get_number_of_files() > 20:
            print 'Trash contains more 20 files. Continue? (y/n)'
            if raw_input() != 'y':
                return

        for trashfile in self.get_trashfiles():
            print ' '*4 + trashfile.oldpath


    def _prepare_index_for_file(self, filename, index):
        if not filename in self.files.keys():
            self.files[filename] = []

        while len(self.files[filename]) <= index:
            self.files[filename].append(None)

        self.files[filename][index] = 'reserved'

    def _reserve_place_for_file(self, filename, index):
        reserve_filepath = os.path.join(
            self.files_path, 
            filename + '[{0}].reserved'.format(index)
        )
        
        with open(reserve_filepath, 'w') as file:
            pass

    def _init_directory(self, path):
        if os.path.exists(path):
            if not os.path.isdir(path):
                raise RMError('Existing path is not dir')

            if not os.access(path, os.R_OK):
                raise RMError('Access denied : ' + path)

        if not os.path.exists(path):
            os.mkdir(path)

    def _init_files(self):
        self.files = {}

        for filename in os.listdir(self.files_path):
            try:
                infopath = os.path.join(self.infos_path, filename + '.trashinfo')
                trashfile = get_trashfile_by_info(infopath)

                pos = filename.rfind('[')
                index = int(filename[pos+1:-1])

                filename = os.path.basename(trashfile.oldpath)

                self._prepare_index_for_file(filename, index)
                self.files[filename][index] = trashfile
            except Exception:
                pass

        self.logger.debug('Trash files initialized')

    def _check_file(self, path):
        if self.config.maxnumber:
            if self.config.maxnumber < self.get_number_of_files() + 1:
                raise RMError('Maximum files in trash')

        if self.config.maxsize:
            if self.config.maxsize < self.get_size_of_trash() + os.path.getsize(path):
                raise RMError('Not enough capacity')


def _is_paths_intersect(path, other_path):
    if path == other_path:
        return True

    if len(path) > len(other_path):
        path, other_path = other_path, path

    return os.path.relpath(other_path, start=path).find('.') == -1;
