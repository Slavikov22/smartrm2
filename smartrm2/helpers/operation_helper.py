#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import os.path
import time
import random
import logging

from smartrm2.rmerror import RMError


def get_trash_operations(trash):
    operations = {}
    for file in trash.get_trashfiles():
        if file.operation_id not in operations.keys():
            operations[file.operation_id] = []

        operations[file.operation_id].append(file)

    return operations


def show_operation(operation_id, trash):
    files = get_operation_files(operation_id, trash)

    del_time = max(
        [ os.path.getctime(trash.get_paths_of_trashfile(file)[0]) 
          for file in files]
    )

    print 'Operation: {0}'.format(operation_id)
    print 'Deleting time: {0}'.format(time.ctime(del_time))

    for file in files:
        print ' '*4 + file.oldpath


def delete_operation(operation_id, trash, oldpath=''):
    logger = logging.getLogger('smartrm2')
    count = 0

    files = get_operation_files(operation_id, trash)
    for trashfile in files:
        if _is_subpath(trashfile.oldpath, oldpath):
            try:
                trash.delete_file(trashfile)
                logger.info('{0} deleted from {1}'\
                            .format(trashfile.oldpath, trash.path))
                count += 1
            except OSError:
                print "Can't delete {0} from {1}"\
                      .format(trashfile.olpdath, trash.path)

    return count


def restore_operation(operation_id, trash, oldpath=''):
    logger = logging.getLogger('smartrm2')
    count = 0

    files = get_operation_files(operation_id, trash)
    for trashfile in sorted(files, key=lambda file: file.oldpath):
        if _is_subpath(trashfile.oldpath, oldpath):
            try:
                trash.restore_trashfile(trashfile)
                logger.info('{0} restored from {1}'\
                            .format(trashfile.oldpath, trash.path))
                count += 1
            except RMError as exc:
                print exc.message

    return count


def get_operation_files(operation_id, trash):
    files = []
    for filename in trash.files.keys():
        for file in trash.files[filename]:
            if file is not None:
                if file.operation_id == operation_id:
                    files.append(file)

    if not files:
        raise RMError('Operation not found')

    return files

def show_operations_of_trash(trash):
    operations = {}
    for file in trash.get_trashfiles():
        del_time = os.path.getctime(trash.get_paths_of_trashfile(file)[0])
        if file.operation_id in operations.keys():
            operations[file.operation_id] = max(
                operations[file.operation_id], del_time
            )
        else:
            operations[file.operation_id] = del_time

    for operation_id in operations.keys():
        print operation_id, time.ctime(operations[operation_id])


def _is_subpath(path, other_path):
    if path == other_path:
        return True;

    return os.path.relpath(path, start=other_path).find('.') == -1
