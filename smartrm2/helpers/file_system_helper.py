#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import os
import os.path

from smartrm2.rmerror import RMError


def check_path_for_remove(path):
    if not os.path.exists(path):
        raise RMError('Path is not exists : ' + path)

    if not os.access(path, os.R_OK):
        raise RMError('Access denied : ' + path)

    if os.path.isdir(path):
        for subpath in get_subpaths(path):
            if not os.access(subpath, os.R_OK):
                raise RMError('Directory is not empty : ' + path)

def get_correct_subpaths(path):
    correct_subpaths = []

    if not os.path.exists(path):
        raise RMError('Path is not exists : ' + path)

    if not os.access(path, os.R_OK):
        raise RMError('Access denied : ' + path)

    if os.path.isfile(path):
        return [path,]

    if os.path.isdir(path):
        for filename in os.listdir(path):
            subpath = os.path.join(path, filename)
            try:
                correct_subpaths.extend(get_correct_subpaths(subpath))
            except RMError: 
                pass

        try: 
            check_path_for_remove(path)
        except RMError: 
            pass
        else: 
            correct_subpaths.append(path) 

        return correct_subpaths


def get_subpaths(path):
    subpaths = []

    for root, dirs, files in os.walk(path):
        for file in files:
            subpaths.append(os.path.join(root, file))

        for directory in dirs:
            subpaths.append(os.path.join(root, directory))

    return subpaths
