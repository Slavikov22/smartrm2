#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

class RMError(Exception):
    def __init__(self, message):
        self.message = message
        