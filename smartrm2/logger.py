#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import logging
import sys
import os
import os.path

from smartrm2.rmerror import RMError


def init_logger(level=logging.CRITICAL, filename=None):
    logger = logging.getLogger('smartrm2')
    logger.setLevel(level)

    if filename:
    	try:
        	handler = logging.FileHandler(filename, mode='w')
        except Exception:
        	raise RMError('Invalid log file')
    else:
        handler = logging.StreamHandler()

    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
