#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import os
import os.path

from smartrm2.formatters.json_formatter import JSONFormatter
from smartrm2.rmerror import RMError


class Config(object):
    def __init__(self, trashpath, restore_policy='passive', autoclear_policies=None, maxsize=None, maxnumber=None):
        self.trashpath = trashpath
        self.restore_policy = restore_policy
        self.autoclear_policies = autoclear_policies
        self.maxsize = maxsize
        self.maxnumber = maxnumber


def get_config_by_file(path, formatter):
    if not os.path.exists(path) or not os.path.isfile(path):
        raise RMError('Invalid config path')

    if not os.access(path, os.R_OK):
        raise RMError('Access denied : ' + path)

    try:
        data = formatter.load(path)
    except Exception:
        raise RMError('Config file incorrect')

    try:
        trashpath = data['trashpath']
    except KeyError:
        raise RMError('Need to choise trash path')

    try:
        restore_policy = data['restore_policy']
    except KeyError:
        restore_policy = None

    try:
        autoclear_policies = data['autoclear_policies']
    except KeyError:
        autoclear_policies = None

    try:
        maxsize = data['maxsize']
    except KeyError:
        maxsize = None

    try:
        maxnumber = data['maxnumber']
    except:
        maxnumber = None

    return Config(
        trashpath=trashpath,
        restore_policy=restore_policy,
        autoclear_policies=autoclear_policies,
        maxsize=maxsize,
        maxnumber=maxnumber,
    )


def get_config_by_user():
    path = os.path.expanduser('~/.trashconfig')

    if os.path.exists(path):
        config = get_config_by_file(path, JSONFormatter())
    else:
        config = Config(os.path.expanduser('~/.trash'))
        save_config(config, path, JSONFormatter())

    return config


def save_config(config, path, formatter):
    data = { field: getattr(config, field) for field in dir(config)
                                           if not field.startswith('__') }

    formatter.save(data, path)
