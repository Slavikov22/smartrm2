#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import time

from smartrm2.trashfile import TrashFile


class ValidationError(Exception):
    def __init__(self, message):
        self.message = message 


def get_validator(policy):
    if policy == 'maxtime':
        return maxtime_validator
    else:
        raise RMError('Validator not found')


def maxtime_validator(trashfile, maxtime):
    if time.time() - trashfile.deletion_time > maxtime:
        raise ValidationError('Maxtime validator raises')
