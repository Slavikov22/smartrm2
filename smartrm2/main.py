#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import logging
import os
import os.path
import random
import sys

from smartrm2.parser import get_parser
from smartrm2.logger import init_logger
from smartrm2.config import get_config_by_file, get_config_by_user
from smartrm2.trash import Trash
from smartrm2.rm import remove_file, remove_tree, get_new_operation_id, _is_equal_regex
from smartrm2.helpers.operation_helper import show_operations_of_trash, show_operation, restore_operation, delete_operation
from smartrm2.helpers.file_system_helper import get_correct_subpaths
from smartrm2.rmerror import RMError
from smartrm2.formatters.json_formatter import JSONFormatter
from smartrm2.formatters.config_formatter import ConfigFormatter

def run():
    parser = get_parser()
    args = parser.parse_args()

    if args.silent:
        sys.stdout = open(os.devnull, 'w')

    if args.log == 0:
        init_logger(level=logging.CRITICAL, filename=args.log_file)
    elif args.log == 1:
        init_logger(level=logging.INFO, filename=args.log_file)
    elif args.log >= 2:
        init_logger(level=logging.DEBUG, filename=args.log_file)

    if args.config:
        if args.formatter == 'json':
            formatter = JSONFormatter()
        else:
            formatter = ConfigFormatter()
        config = get_config_by_file(args.config, formatter)
    else:
        config = get_config_by_user()

    if args.trash_path:
        config.trashpath = args.trash_path

    if args.restore_policy:
        config.restore_policy = args.restore_policy

    if args.maxnumber:
        config.maxnumber = args.maxnumber

    if args.maxsize:
        config.maxsize = args.maxsize

    if args.maxtime:
        if config.autoclear_policies:
            config.autoclear_policies['maxtime'] = args.maxtime
        else:
            config.autoclear_policies = {}
            config.autoclear_policies['maxtime'] = args.maxtime

    trash = None
    if args.trash:
        trash = Trash(config)

    if args.clear:
        if not trash:
            raise RMError('Need to choise trash')

        file_number = trash.get_number_of_files()

        if args.confirm:
            print 'Clear trash? (Deleting {0} files) (y/n)'\
                  .format(file_number)
            if raw_input() != 'y':
                raise RMError('Operation aborted')

        try:
            trash.clear()
        except OSError:
            raise RMError("Trash can't be cleared")
        
        print 'Deleted {0} files from {1}'.format(file_number, trash.path)
        return

    if args.restore or args.delete:
        if not args.operation or args.operation == -1:
            raise RMError('Need to choise operation')

        if not trash:
            raise RMError('Need to choise trash')

        if args.restore:
            if args.confirm:
                print 'Restore files? (y/n)'
                if raw_input() != 'y':
                    raise RMError('Operation aborted')

            count = 0
            if not args.paths:
                count += restore_operation(args.operation, trash)

            else:
                for path in args.paths:
                    count += restore_operation(args.operation, trash, path)
            print 'Restored {0} files from {1}'.format(count, trash.path)
            return

        if args.delete:
            if args.confirm:
                print 'Delete files? (y/n)'
                if raw_input() != 'y':
                    raise RMError('Operation aborted')

            count = 0
            if not args.paths:
                count += delete_operation(args.operation, trash)
            else:
                for path in args.paths:
                    count += delete_operation(args.operation, trash, path)
            print 'Deleted {0} files from {1}'.format(count, trash.path)
            return

    if args.operation:
        if not trash:
            raise RMError('Need to choise trash')

        if args.operation == -1:
            show_operations_of_trash(trash)
        else:
            show_operation(args.operation, trash)
        return

    if trash and not args.paths:
        trash.show()
        return
    
    operation_id = get_new_operation_id()

    correct_subpaths = []
    for path in args.paths:
        try:
            correct_subpaths.extend(get_correct_subpaths(path))
        except RMError:
            pass

    if args.regex:
        correct_subpaths = [ subpath for subpath in correct_subpaths
                             if _is_equal_regex(subpath, args.regex)]

    if args.confirm:
        print 'Remove {0} files? (y/n)'.format(len(correct_subpaths))
        if raw_input() != 'y':
            raise RMError('Operation aborted')

    for path in args.paths:
        try:
            if os.path.isdir(path):
                remove_tree(
                    path=path, 
                    regex=args.regex, 
                    trash=trash, 
                    dry_run=args.dry_run, 
                    operation_id=operation_id
                )
            else:
                remove_file(
                    path=path, 
                    trash=trash, 
                    dry_run=args.dry_run,
                    operation_id=operation_id
                )
        except RMError as exc:
            print exc.message

    print 'Removed {0} files.'.format(len(correct_subpaths))

    return


def main():
    try:
        run()
    except RMError as exc:
        print exc.message
        sys.exit(1)

    sys.stdout.flush()

    sys.exit(0)
