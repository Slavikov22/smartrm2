#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import os
import os.path
import time
import ConfigParser

from smartrm2.rmerror import RMError


class TrashFile(object):
    def __init__(self, oldpath, size, deletion_time, operation_id=0):
        self.oldpath = oldpath
        self.size=size
        self.deletion_time=deletion_time
        self.operation_id = operation_id

    def __eq__(self, other):
        if self is None or other is None:
            return False

        return ((self.oldpath == other.oldpath) and
                (self.size == other.size) and
                (self.deletion_time == other.deletion_time) and
                (self.operation_id == other.operation_id))


def get_trashfile_by_file(path):
    if not os.path.exists(path):
        raise RMError('File is not exists : ' + path)

    return TrashFile(
        oldpath=os.path.abspath(path),
        size=os.path.getsize(path),
        deletion_time=int(time.time()),
    )


def get_trashfile_by_info(path):
    parser = ConfigParser.RawConfigParser()
    parser.read(path)

    return TrashFile(
        oldpath=parser.get('Trash Info', 'oldpath'),
        size=int(parser.get('Trash Info', 'size')),
        deletion_time=int(parser.get('Trash Info', 'deletion_time')),
        operation_id=int(parser.get('Trash Info', 'operation_id')),
    )
    

def create_info(infopath, trashfile):
    parser = ConfigParser.RawConfigParser()

    parser.add_section('Trash Info')
    parser.set('Trash Info', 'oldpath', trashfile.oldpath)
    parser.set('Trash Info', 'size', trashfile.size)
    parser.set('Trash Info', 'deletion_time', trashfile.deletion_time)
    parser.set('Trash Info', 'operation_id', trashfile.operation_id)

    with open(infopath, "w") as infofile:
        parser.write(infofile)
        