#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import os
import os.path
import json


class JSONFormatter(object):
    def load(self, path):
        decoder = json.JSONDecoder()

        with open(path, 'r') as file:
            data = decoder.decode(file.read())

        return data


    def save(self, data, path):
        encoder = json.JSONEncoder(indent=4)

        with open(path, 'w') as file:
            file.write(encoder.encode(data))
    