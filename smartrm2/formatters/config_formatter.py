#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import ConfigParser

class ConfigFormatter(object):
    def load(self, path):
        data = {}

        parser = ConfigParser.ConfigParser()
        parser.read(path)

        for section in parser.sections():
            if section == 'config':
                for option in parser.options(section):
                    data[option] = parser.get(section, option)

                    if data[option].isdigit():
                        data[option] = int(data[option])
            else:
                data[section] = {}
                for option in parser.options(section):  
                    data[section][option] = parser.get(section, option)

                    if data[section][option].isdigit():
                        data[section][option] = int(data[section][option])

        return data

    def save(self, data, path):
        parser = ConfigParser.ConfigParser()
        parser.add_section('config')

        for key in data.keys():
            if isinstance(data[key], dict):
                parser.add_section(key)
                for subkey in data[key].keys():
                    parser.set(key, subkey, data[key][subkey])
            else:
                parser.set('config', key, data[key])

        with open(path, 'w') as file:
            parser.write(file)
       