#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import argparse

def get_parser():
    parser = argparse.ArgumentParser(
        prog='smartrm2',
        add_help=False,
    )

    parser.add_argument(
        'paths', 
        nargs='*',
    )

    parser.add_argument(
        '-c',
        '--config',
        default='',
        metavar='PATH_TO_CONFIG'
    )

    parser.add_argument(
        '--formatter',
        choices=['json', 'config'],
        default='json',
    )

    parser.add_argument(
        '-t',
        '--trash',
        action='store_true',
    )

    parser.add_argument(
        '--regex',
        default='.*',
    )

    parser.add_argument(
        '-r',
        '--restore',
        action='store_true',
    )

    parser.add_argument(
        '--restore-policy',
        dest='restore_policy',
        choices=['passive', 'aggressive'],
        default=None,
    )

    parser.add_argument(
        '-d',
        '--delete',
        action='store_true',
    )

    parser.add_argument(
        '--clear',
        action='store_true',
    )

    parser.add_argument(
        '-o',
        '--operation',
        nargs='?',
        default=0,
        const=-1,
        type=int,
    )

    parser.add_argument(
        '--dry-run',
        dest='dry_run',
        action='store_true',
    )

    parser.add_argument(
        '-l',
        '--log',
        action='count',
    )

    parser.add_argument(
        '--log-file',
        dest='log_file',
        default='',
    )

    parser.add_argument(
        '--trash-path',
        dest='trash_path',
        default='',
    )

    parser.add_argument(
        '--maxnumber',
        default=0,
        type=int,
    )

    parser.add_argument(
        '--maxsize',
        default=0,
        type=int,
    )

    parser.add_argument(
        '--maxtime',
        default=0,
        type=int,
    )

    parser.add_argument(
        '--version',
        action='version',
        version='%(prog) 1.0',
    )

    print_group = parser.add_mutually_exclusive_group()

    print_group.add_argument(
        '--confirm',
        action='store_true',
    )

    print_group.add_argument(
        '--silent',
        action='store_true',
    )

    return parser
