#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name='smartrm2',
    version='1.0',
    author='Slava Yakovlev',
    author_email='Slavikov22@mail.ru',
    packages=find_packages(),
    entry_points={
        'console_scripts': ['smartrm2 = smartrm2.main:main']
    }
)
