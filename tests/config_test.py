#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import unittest
import os.path
import shutil
import tempfile

from smartrm2.formatters.config_formatter import ConfigFormatter
from smartrm2.formatters.json_formatter import JSONFormatter
from smartrm2.config import Config, get_config_by_file, get_config_by_user, save_config

class ConfigTest(unittest.TestCase):
    def setUp(self):
        self.dirpath = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.dirpath)

    def test_save_and_load_config(self):
        config = Config(
            trashpath='/tmp/config',
            maxsize=1024,
        )

        save_config(config, os.path.join(self.dirpath, 'config'), ConfigFormatter())
        save_config(config, os.path.join(self.dirpath, 'json'), JSONFormatter())

        config_config = get_config_by_file(os.path.join(self.dirpath, 'config'), ConfigFormatter())
        config_json = get_config_by_file(os.path.join(self.dirpath, 'json'), JSONFormatter())

        self.assertTrue(config_config.maxsize == 1024, 'Config formatter not work')
        self.assertTrue(config_json.maxsize == 1024, 'Json formatter not work')

    def test_get_config_by_user(self):
        config = get_config_by_user()


if __name__ == '__main__':
    unittest.main()
    