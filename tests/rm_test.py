#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import unittest
import tempfile
import os
import os.path
import shutil
import time

from smartrm2.rm import remove_file, remove_tree
from smartrm2.trash import Trash
from smartrm2.config import Config
from smartrm2.trashfile import get_trashfile_by_file
from smartrm2.helpers.operation_helper import restore_operation


class RMTest(unittest.TestCase):
    def setUp(self):
        trashpath = tempfile.mkdtemp()
        config = Config(trashpath)
        self.trash = Trash(config)
        self.trash.clear()

    def tearDown(self):
        shutil.rmtree(self.trash.path)

    def test_remove_file(self):
        testfile = tempfile.NamedTemporaryFile(delete=False)
    
        remove_file(testfile.name)
        self.assertFalse(os.path.exists(testfile.name))

    def test_remove_file_with_trash(self):
        testfile = tempfile.NamedTemporaryFile(delete=False)
        filename = os.path.basename(testfile.name)

        remove_file(testfile.name, trash=self.trash)
        self.assertFalse(os.path.exists(testfile.name))

        newpath = os.path.join(self.trash.files_path, filename + '[0]')
        self.assertTrue(os.path.exists(newpath))

    def test_remove_directory(self):
        testdir = tempfile.mkdtemp()
    
        remove_file(testdir)
        self.assertFalse(os.path.exists(testdir))

    def test_remove_directory_with_trash(self):
        testdir = tempfile.mkdtemp()
        dirname = os.path.basename(testdir)

        remove_file(testdir, trash=self.trash)
        self.assertFalse(os.path.exists(testdir))

        newpath = os.path.join(self.trash.files_path, dirname + '[0]')
        self.assertTrue(os.path.exists(newpath))

    def test_remove_tree(self):
        testroot = tempfile.mkdtemp()
        
        testdir_1 = tempfile.mkdtemp(dir=testroot)
        testdir_2 = tempfile.mkdtemp(dir=testroot)
        testdir_3 = tempfile.mkdtemp(dir=testroot)

        testfile = tempfile.NamedTemporaryFile(dir=testroot, delete=False)
        testfile_1 = tempfile.NamedTemporaryFile(dir=testdir_1, delete=False)
        testfile_1_1 = tempfile.NamedTemporaryFile(dir=testdir_1, delete=False)

        testdir_3_1 = tempfile.mkdtemp(dir=testdir_3)
        testdir_3_1_1 = tempfile.mkdtemp(dir=testdir_3_1)

        remove_tree(testroot)
        self.assertFalse(os.path.exists(testroot))

    def test_remove_tree_with_trash(self):
        testroot = tempfile.mkdtemp()
        
        testdir_1 = tempfile.mkdtemp(dir=testroot)
        testdir_2 = tempfile.mkdtemp(dir=testroot)
        testdir_3 = tempfile.mkdtemp(dir=testroot)

        testfile = tempfile.NamedTemporaryFile(dir=testroot, delete=False)
        testfile_1 = tempfile.NamedTemporaryFile(dir=testdir_1, delete=False)
        testfile_1_1 = tempfile.NamedTemporaryFile(dir=testdir_1, delete=False)

        testdir_3_1 = tempfile.mkdtemp(dir=testdir_3)
        testdir_3_1_1 = tempfile.mkdtemp(dir=testdir_3_1)

        remove_tree(testroot, trash=self.trash)
        self.assertFalse(os.path.exists(testroot))

        self.assertTrue(len(os.listdir(self.trash.files_path)) == 9)

    def test_remove_by_regex(self):
        testroot = tempfile.mkdtemp()

        open(os.path.join(testroot, 'a'), 'w')
        open(os.path.join(testroot, 'bb'), 'w')
        open(os.path.join(testroot, 'c'), 'w')

        testdir_1 = os.path.join(testroot, 'kk')
        testdir_2 = os.path.join(testroot, 'q')
        
        os.mkdir(testdir_1)
        os.mkdir(testdir_2)

        open(os.path.join(testdir_1, 'v'), 'w')
        open(os.path.join(testdir_2, 'mmm'), 'w')

        remove_tree(testroot, regex='.')

        self.assertFalse(os.path.exists(os.path.join(testroot, 'a')))
        self.assertTrue(os.path.exists(os.path.join(testroot, 'bb')))
        self.assertFalse(os.path.exists(os.path.join(testroot, 'c')))

        self.assertTrue(os.path.exists(testdir_1))
        self.assertFalse(os.path.exists(testdir_2))

        self.assertFalse(os.path.exists(os.path.join(testdir_1, 'v')))

        shutil.rmtree(testroot)

    def test_remove_by_regex_with_trash(self):
        testroot = tempfile.mkdtemp()

        open(os.path.join(testroot, 'a'), 'w')
        open(os.path.join(testroot, 'bb'), 'w')
        open(os.path.join(testroot, 'c'), 'w')

        testdir_1 = os.path.join(testroot, 'kk')
        testdir_2 = os.path.join(testroot, 'q')
        
        os.mkdir(testdir_1)
        os.mkdir(testdir_2)

        open(os.path.join(testdir_1, 'v'), 'w')
        open(os.path.join(testdir_2, 'mmm'), 'w')

        remove_tree(testroot, regex='.', trash=self.trash)

        self.assertFalse(os.path.exists(os.path.join(testroot, 'a')))
        self.assertTrue(os.path.exists(os.path.join(testroot, 'bb')))
        self.assertFalse(os.path.exists(os.path.join(testroot, 'c')))

        self.assertTrue(os.path.exists(testdir_1))
        self.assertFalse(os.path.exists(testdir_2))

        self.assertFalse(os.path.exists(os.path.join(testdir_1, 'v')))

        self.assertTrue(len(os.listdir(self.trash.files_path)) == 5)

        shutil.rmtree(testroot)

    def test_restore_file(self):
        testfile = tempfile.NamedTemporaryFile()
        trashfile = get_trashfile_by_file(testfile.name)

        operation_id = remove_file(testfile.name, trash=self.trash)

        restore_operation(operation_id, self.trash, trashfile.oldpath)

        self.assertTrue(os.path.exists(testfile.name))

    def test_restore_operation(self):
        testroot = tempfile.mkdtemp()
        
        testdir_1 = tempfile.mkdtemp(dir=testroot)
        testdir_2 = tempfile.mkdtemp(dir=testroot)
        testdir_3 = tempfile.mkdtemp(dir=testroot)

        testfile = tempfile.NamedTemporaryFile(dir=testroot, delete=False)
        testfile_1 = tempfile.NamedTemporaryFile(dir=testdir_1, delete=False)
        testfile_1_1 = tempfile.NamedTemporaryFile(dir=testdir_1, delete=False)

        testdir_3_1 = tempfile.mkdtemp(dir=testdir_3)
        testdir_3_1_1 = tempfile.mkdtemp(dir=testdir_3_1)

        operation_id = remove_tree(testroot, trash=self.trash)
        restore_operation(operation_id, self.trash, '/')

        self.assertTrue(os.path.exists(testroot))
        self.assertTrue(os.path.exists(testdir_1))
        self.assertTrue(os.path.exists(testdir_2))
        self.assertTrue(os.path.exists(testdir_3))
        self.assertTrue(os.path.exists(testfile.name))
        self.assertTrue(os.path.exists(testfile_1.name))
        self.assertTrue(os.path.exists(testfile_1_1.name))
        self.assertTrue(os.path.exists(testdir_3_1))
        self.assertTrue(os.path.exists(testdir_3_1_1))

        self.assertFalse(os.listdir(self.trash.files_path))
        self.assertFalse(os.listdir(self.trash.infos_path))

        shutil.rmtree(testroot)

    def test_remove_big_tree_not_effective(self):
        NUM_FOLDERS = 4
        NUM_FILES_IN_FOLDER = 20000

        testroot = tempfile.mkdtemp()

        for i in range(NUM_FOLDERS):
            testdir = tempfile.mkdtemp(dir=testroot)
            
            for j in range(NUM_FILES_IN_FOLDER):
                testfile = tempfile.NamedTemporaryFile(dir=testdir, delete=False)

        t = time.time()

        remove_tree(testroot, processes_count=1)

        print 'Total(1 process):', time.time() - t
        self.assertFalse(os.path.exists(testroot))

    def test_remove_big_tree(self):
        NUM_FOLDERS = 4
        NUM_FILES_IN_FOLDER = 20000

        testroot = tempfile.mkdtemp()

        for i in range(NUM_FOLDERS):
            testdir = tempfile.mkdtemp(dir=testroot)
            
            for j in range(NUM_FILES_IN_FOLDER):
                testfile = tempfile.NamedTemporaryFile(dir=testdir, delete=False)

        t = time.time()

        remove_tree(testroot, processes_count=4)

        print 'Total(4 processes):', time.time() - t
        self.assertFalse(os.path.exists(testroot))

    def test_remove_big_tree_with_trash_not_effective(self):
        NUM_FOLDERS = 4
        NUM_FILES_IN_FOLDER = 4000

        testroot = tempfile.mkdtemp()

        for i in range(NUM_FOLDERS):
            testdir = tempfile.mkdtemp(dir=testroot)
            
            for j in range(NUM_FILES_IN_FOLDER):
                testfile = tempfile.NamedTemporaryFile(dir=testdir, delete=False)

        t = time.time()

        remove_tree(testroot, trash=self.trash, processes_count=1)

        print 'Total with trash (1 process):', time.time() - t
        self.assertFalse(os.path.exists(testroot))

        self.assertTrue(
            len(os.listdir(self.trash.files_path)) == \
                NUM_FILES_IN_FOLDER*NUM_FOLDERS + NUM_FOLDERS + 1
        )

    def test_remove_big_tree_with_trash(self):
        NUM_FOLDERS = 4
        NUM_FILES_IN_FOLDER = 4000

        testroot = tempfile.mkdtemp()

        for i in range(NUM_FOLDERS):
            testdir = tempfile.mkdtemp(dir=testroot)
            
            for j in range(NUM_FILES_IN_FOLDER):
                testfile = tempfile.NamedTemporaryFile(dir=testdir, delete=False)

        t = time.time()

        remove_tree(testroot, trash=self.trash, processes_count=4)

        print 'Total with trash(4 processes):', time.time() - t
        self.assertFalse(os.path.exists(testroot))

        self.assertTrue(
            len(os.listdir(self.trash.files_path)) == \
                NUM_FILES_IN_FOLDER*NUM_FOLDERS + NUM_FOLDERS + 1
        )


if __name__ == '__main__':
    unittest.main()
    