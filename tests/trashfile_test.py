#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import unittest
import tempfile
import time

from smartrm2.trashfile import get_trashfile_by_file, get_trashfile_by_info, create_info

class TrashFileTest(unittest.TestCase):
    def setUp(self):
        self.testfile = tempfile.NamedTemporaryFile()
        with open(self.testfile.name, 'w') as testfile:
            testfile.write('a'*100)

    def test_create_trashfile(self):
        trashfile = get_trashfile_by_file(self.testfile.name)

        self.assertTrue(trashfile.oldpath == self.testfile.name)
        self.assertTrue(trashfile.size > 0)
        self.assertTrue(time.time() - trashfile.deletion_time < 3)

    def test_save_info(self):
        trashfile = get_trashfile_by_file(self.testfile.name)
        infofile = tempfile.NamedTemporaryFile()

        create_info(infofile.name, trashfile)

        load_trashfile = get_trashfile_by_info(infofile.name)

        self.assertTrue(load_trashfile.oldpath == trashfile.oldpath)
        self.assertTrue(load_trashfile.size == trashfile.size)
        self.assertTrue(load_trashfile.deletion_time == trashfile.deletion_time)

if __name__ == '__main__':
    unittest.main()
