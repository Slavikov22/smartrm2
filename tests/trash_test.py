#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import unittest
import tempfile
import os
import os.path
import shutil
import time

from smartrm2.trash import Trash
from smartrm2.config import Config
from smartrm2.rmerror import RMError

class TrashTest(unittest.TestCase):
    def setUp(self):
        trashpath = tempfile.mkdtemp()
        config = Config(trashpath)
        self.trash = Trash(config)
        self.trash.clear()

    def tearDown(self):
        shutil.rmtree(self.trash.path)

    def test_create_trash(self):
        files_path = os.path.join(self.trash.path, 'files')
        infos_path = os.path.join(self.trash.path, 'info')

        self.assertTrue(os.path.exists(files_path))
        self.assertTrue(os.path.exists(infos_path))

    def test_move_file_to_trash(self):
        testfile = tempfile.NamedTemporaryFile(delete=False)
        filename = os.path.basename(testfile.name)

        self.trash.move_file_to_trash(testfile.name)
        
        self.assertTrue(self.trash.files[filename][0].oldpath == testfile.name)

    def test_move_file_to_trash_with_get_index(self):
        testfile = tempfile.NamedTemporaryFile(delete=False)
        filename = os.path.basename(testfile.name)

        index = self.trash.get_free_index(filename)

        self.trash.move_file_to_trash(path=testfile.name, index=index)

        self.assertFalse(os.path.exists(testfile.name))

        newpath = os.path.join(self.trash.files_path, 
                               filename + '[{0}]'.format(index))

        self.assertTrue(os.path.exists(newpath))

    def test_move_dir_to_trash(self):
        testdir = tempfile.mkdtemp()
        dirname = os.path.basename(testdir)

        self.trash.move_file_to_trash(testdir)

        self.assertFalse(os.path.exists(testdir))
        self.assertTrue(self.trash.files[dirname][0].oldpath == testdir)

    def test_delete_file(self):
        testfile = tempfile.NamedTemporaryFile(delete=False)
        filename = os.path.basename(testfile.name)
        index = self.trash.get_free_index(filename)

        self.trash.move_file_to_trash(testfile.name, index=index)

        trashfile = self.trash.files[filename][index]

        self.trash.delete_file(trashfile)

        self.assertFalse(self.trash.files[filename][index])
        self.assertFalse(os.path.exists(
            os.path.join(self.trash.files_path, filename + '[{0}]'.format(index))
        ))
        self.assertFalse(os.path.exists(
            os.path.join(self.trash.infos_path, filename + '[{0}]'.format(index) + '.trashinfo')
        ))

    def test_clear(self):
        testfile_1 = tempfile.NamedTemporaryFile(delete=False)
        testfile_2 = tempfile.NamedTemporaryFile(delete=False)
        testfile_3 = tempfile.NamedTemporaryFile(delete=False)

        testdir = tempfile.mkdtemp()

        self.trash.move_file_to_trash(testfile_1.name)
        self.trash.move_file_to_trash(testfile_2.name)
        self.trash.move_file_to_trash(testfile_3.name)
        self.trash.move_file_to_trash(testdir)

        self.trash.clear()

        self.assertFalse(os.listdir(self.trash.files_path))
        self.assertFalse(os.listdir(self.trash.infos_path))

        for filename in self.trash.files.keys():
            for trashfile in self.trash.files[filename]:
                self.assertIsNone(trashfile)

    def test_maxsize(self):
        self.trash.config.maxsize = 1000

        testfile_1 = tempfile.NamedTemporaryFile(delete=False)
        testfile_2 = tempfile.NamedTemporaryFile()
        testfile_3 = tempfile.NamedTemporaryFile(delete=False)

        with open(testfile_1.name, 'w') as file:
            file.write('a')

        with open(testfile_2.name, 'w') as file:
            file.write('a'*1000)

        with open(testfile_3.name, 'w') as file:
            file.write('a')

        self.trash.move_file_to_trash(testfile_1.name)

        with self.assertRaises(RMError):
            self.trash.move_file_to_trash(testfile_2.name)
        
        self.trash.move_file_to_trash(testfile_3.name)

    def test_maxnumber(self):
        self.trash.config.maxnumber = 1

        testfile_1 = tempfile.NamedTemporaryFile(delete=False)
        testfile_2 = tempfile.NamedTemporaryFile()
        testfile_3 = tempfile.NamedTemporaryFile()

        self.trash.move_file_to_trash(testfile_1.name)

        with self.assertRaises(RMError):
            self.trash.move_file_to_trash(testfile_2.name)

        with self.assertRaises(RMError):
            self.trash.move_file_to_trash(testfile_3.name)

    def test_maxtime(self):
        self.trash.config.autoclear_policies = {}
        self.trash.config.autoclear_policies['maxtime'] = 1

        testfile = tempfile.NamedTemporaryFile(delete=False)
        filename = os.path.basename(testfile.name)

        self.trash.move_file_to_trash(testfile.name)

        self.trash.start_autoclear()

        self.assertTrue(self.trash.files[filename][0].oldpath == testfile.name)

        time.sleep(1)

        self.trash.start_autoclear()

        self.assertFalse(self.trash.files[filename][0])
        self.assertFalse(os.listdir(self.trash.files_path))
        self.assertFalse(os.listdir(self.trash.infos_path))

    def test_restore_trashfile(self):
        testfile = tempfile.NamedTemporaryFile()
        filename = os.path.basename(testfile.name)

        testdir = tempfile.mkdtemp()
        dirname = os.path.basename(testdir)

        self.trash.move_file_to_trash(testfile.name)

        self.trash.move_file_to_trash(testdir)

        self.trash.restore_trashfile(self.trash.files[filename][0])
        self.trash.restore_trashfile(self.trash.files[dirname][0])

        self.assertFalse(self.trash.files[filename][0])
        self.assertFalse(self.trash.files[dirname][0])

        self.assertFalse(os.listdir(self.trash.files_path))
        self.assertFalse(os.listdir(self.trash.infos_path))

        self.assertTrue(os.path.exists(testfile.name))
        self.assertTrue(os.path.exists(testdir))

        os.rmdir(testdir)

    def test_reload_trash(self):
        testfile = tempfile.NamedTemporaryFile(delete=False)
        filename = os.path.basename(testfile.name)

        testdir = tempfile.mkdtemp()
        dirname = os.path.basename(testdir)

        self.trash.move_file_to_trash(testfile.name)
        self.trash.move_file_to_trash(testdir)

        config = self.trash.config
        self.trash = Trash(config)

        self.assertTrue(self.trash.files[filename][0])
        self.assertTrue(self.trash.files[dirname][0])

if __name__ == '__main__':
    unittest.main()